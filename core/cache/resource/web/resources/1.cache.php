<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 1,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Главная',
    'longtitle' => 'Поздравляем!',
    'description' => '',
    'alias' => 'index',
    'alias_visible' => 1,
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => NULL,
    'content' => '<p>You have successfully installed MODX Revolution&nbsp;[[++settings_version]]!</p>
<p>Now that MODX is installed you can login to the manager to create your templates, manage content and install third party extras to add functionality to your&nbsp;website. </p>

<h2>New to&nbsp;MODX?</h2>

<p>Pages on a MODX site are called <a href="https://rtfm.modx.com/revolution/2.x/making-sites-with-modx/structuring-your-site/resources">Resources</a>, and are visible on the left-hand side of the manager in the Resources tab. Resources can be nested under other resources, making it easy to create a tree of resources. There are different types of resources for different use&nbsp;cases.</p>

<p>Building your website is done through a combination of <b>Templates</b>, <b>Template Variables</b>, <b>Chunks</b>, <b>Snippets</b> and <b>Plugins</b>. Collectively these are known as <b>Elements</b>, and can also be found in the left-hand side of the manager, in the Elements&nbsp;tab.</p>

<p><a href="https://rtfm.modx.com/revolution/2.x/making-sites-with-modx/structuring-your-site/templates">Templates</a> contain the outer markup of any page. Each resource can only be assigned to a single template at a time. By adding <a href="https://rtfm.modx.com/revolution/2.x/making-sites-with-modx/customizing-content/template-variables">Template Variables</a> to a template, you can add custom fields for any resource using that particular&nbsp;template.</p>

<p>With <a href="https://rtfm.modx.com/revolution/2.x/making-sites-with-modx/structuring-your-site/chunks">Chunks</a> you can share parts of the markup, such as a header, across different templates. <a href="https://rtfm.modx.com/revolution/2.x/making-sites-with-modx/structuring-your-site/using-snippets">Snippets</a> are pieces of PHP that return dynamic content, such as summaries of other resources or the current date. With snippets, you will often use Chunks to mark up the pieces of content it returns, instead of mixing the PHP and&nbsp;HTML.</p>

<p>Finally, <a href="https://rtfm.modx.com/revolution/2.x/developing-in-modx/basic-development/plugins">Plugins</a> enable more advanced features by hooking into the extensive events system provided by&nbsp;MODX.</p>

<p>To learn more about MODX, be sure to check out the <a href="https://rtfm.modx.com/revolution/2.x/getting-started">Getting Started</a> section in the official&nbsp;documentation.</p>
',
    'richtext' => 1,
    'template' => 1,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1587387545,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 0,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => NULL,
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    '_content' => '<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#141414" />
    <meta name="KeyWords" content=""/>
    <meta name="Description" content=""/>
<!--    <link rel="icon" href="favicon.ico" type="image/x-icon">-->
<!--    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">-->
    <link rel="stylesheet" href="/assets/css/style1.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/responsive.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.0.0/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
    <script src="https://kit.fontawesome.com/bf263f8154.js" crossorigin="anonymous"></script>
    <script src="/assets/js/jquery331.js"></script>
    <script src="/assets/js/jui.js"></script>
    <script src="/assets/slick/slick.js"></script>
</head>
<body>

<div id="mobileMenu">
    <div id="mobileMenuClose"></div>
    <a href="#">Услуги</a>
    <a href="#">Цены</a>
    <a href="#">Портфолио</a>
    <a href="#">Блог</a>
    <a href="#">Контакты</a>
    <a href="#">О нас</a>
</div>

<div id="headerWrap">
    <div id="header">
        <a href="/" id="logo"><img src="/assets/images/logo.png"></a>
        <a href="#" class="menuItem">Услуги<span></span></a>
        <a href="#" class="menuItem">Цены<span></span></a>
        <a href="#" class="menuItem">Портфолио<span></span></a>
        <a href="#" class="menuItem">Блог<span></span></a>
        <a href="#" class="menuItem">Контакты<span></span></a>
        <a href="#" class="menuItem">О нас<span></span></a>

        <div id="headerContacts">
            <div class="headerContact"><img src="/assets/images/phone.png"><span>8 (911) 925 66 52</span></div>
            <div class="headerContact"><img src="/assets/images/mail.png"><span>info@designa8.ru</span></div>
        </div>


        <div id="mobMenuBtn"></div>
    </div>

    <div id="headerSocials">
        <a href="https://vk.com/designa8" target="_blank"><img src="/assets/images/socVk.png"></a>
        <a href="https://www.facebook.com/www.a8.ru/" target="_blank"><img src="/assets/images/socFb.png"></a>
        <a href="https://www.instagram.com/a8.design/" target="_blank"><img src="/assets/images/socIg.png"></a>
        <a href="#"><img src="/assets/images/socPi.png" target="_blank"></a>
    </div>
</div>

<div id="homeSection">
    <div id="hsOvr">
        <div id="nameBlock" style="margin-top: 300px;">
            <p id="nbBig">Студия дизайна интерьера</p>
            <p id="nbSm">Архитектурное проектирование</p>
            <p style="font-size: 16px; margin-top: 70px;">Сэкономь полгода жизни, заказав дизайн проект по авторской методике</p>
            <p style="font-size: 16px">от архитектора из Академии художеств с 9 летним опытом работы.</p>
            <a class="btn-gold btn-lg btn-theme" href="#" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 55px;">
                <span class="btn-gold-inside">Получить каталог топ проектов</span>
            </a>
        </div>
        <div class="mouse" style="margin-top: 50px; margin-left: 47%;"></div>
    </div>

</div>

<div id="featsSection">
    <div class="sContent">
        <div class="luCrn"></div>
        <div class="rbCrn"></div>
        <div id="features">
            <div class="feature"><span class="fNum">+250</span><span class="fText">Проектов в России</span></div>
            <div class="feature"><span class="fNum">+68</span><span class="fText">Реализованных объектов</span></div>
            <div class="feature"><span class="fNum">+12</span><span class="fText">Международных объектов</span></div>
            <div class="feature"><span class="fNum">+9</span><span class="fText">Лет работы</span></div>
        </div>
    </div>
</div>

<div class="cnHeaderA"><span>Наши проекты:</span></div>
<div id="projectsSection">
    <div id="projects">
        <div class="project"><img src="/assets/images/prj1.jpg"><div class="prjOvr"></div><div class="prjTitle">Интерьер с чёрными акцентами<i></i><span>Москва</span></div><div class="prjDscr"><div class="prjSqr">127 <i>м<sup>2</sup></i></div><p>Дизайн интерьера этой квартиры выполнен в стиле минимализм.</p><p>Динамику пространству придаёт обилие используемых фактур и зонирование посредством конструктивных элементов.</p></div><a href="#" class="prjMore">Подробнее</a><div class="ruCrn"></div><div class="lbCrn"></div></div>
        <div class="project"><img src="/assets/images/prj2.jpg"><div class="prjOvr"></div><div class="prjTitle">Эклектика в сердце Петербурга<i></i><span>Санкт-Петебург</span></div><div class="prjDscr"><div class="prjSqr">148 <i>м<sup>2</sup></i></div><p>Эта светлая квартира, площадью почти 150 м2, находится в самом центре Санкт-Петербурга, В ЖК «Парадный квартал».</p><p>Перед нашей командой стояла задача грамотного зонирования пространства и создания воздушного и легкого настроения в интерьере.</p></div><a href="#" class="prjMore">Подробнее</a><div class="ruCrn"></div><div class="lbCrn"></div></div>
        <div class="project"><img src="/assets/images/prj3.jpg"><div class="prjOvr"></div><div class="prjTitle">Дом в коттеджном посёлке Honkanova в Солнечном<i></i><span>Санкт-Петебург</span></div><div class="prjDscr"><div class="prjSqr">360 <i>м<sup>2</sup></i></div><p>Проект дома, площадью 360 м2, является частью элитного посёлка Honkanova, который располагается в Солнечном, на берегу Финского залива.</p></div><a href="#" class="prjMore">Подробнее</a><div class="ruCrn"></div><div class="lbCrn"></div></div>
    </div>
    <div id="projects">
        <div class="project"><img src="/assets/images/prj1.jpg"><div class="prjOvr"></div><div class="prjTitle">Интерьер с чёрными акцентами<i></i><span>Москва</span></div><div class="prjDscr"><div class="prjSqr">127 <i>м<sup>2</sup></i></div><p>Дизайн интерьера этой квартиры выполнен в стиле минимализм.</p><p>Динамику пространству придаёт обилие используемых фактур и зонирование посредством конструктивных элементов.</p></div><a href="#" class="prjMore">Подробнее</a><div class="ruCrn"></div><div class="lbCrn"></div></div>
        <div class="project"><img src="/assets/images/prj2.jpg"><div class="prjOvr"></div><div class="prjTitle">Эклектика в сердце Петербурга<i></i><span>Санкт-Петебург</span></div><div class="prjDscr"><div class="prjSqr">148 <i>м<sup>2</sup></i></div><p>Эта светлая квартира, площадью почти 150 м2, находится в самом центре Санкт-Петербурга, В ЖК «Парадный квартал».</p><p>Перед нашей командой стояла задача грамотного зонирования пространства и создания воздушного и легкого настроения в интерьере.</p></div><a href="#" class="prjMore">Подробнее</a><div class="ruCrn"></div><div class="lbCrn"></div></div>
        <div class="project"><img src="/assets/images/prj3.jpg"><div class="prjOvr"></div><div class="prjTitle">Дом в коттеджном посёлке Honkanova в Солнечном<i></i><span>Санкт-Петебург</span></div><div class="prjDscr"><div class="prjSqr">360 <i>м<sup>2</sup></i></div><p>Проект дома, площадью 360 м2, является частью элитного посёлка Honkanova, который располагается в Солнечном, на берегу Финского залива.</p></div><a href="#" class="prjMore">Подробнее</a><div class="ruCrn"></div><div class="lbCrn"></div></div>
    </div>
    <a href="#" id="seePortfolio">Посмотреть все работы</a>
</div>

<div id="more" style="padding: 50px; padding-left: 0px; padding-right: 0px">
    <div class="sContent">
        <p style="font-weight: bold; font-size: 40px; color: white">Хочу так же!</p>
        <a class="btn-gold btn-lg btn-theme" href="#" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 55px;">
            <span class="btn-gold-inside">Получить каталог топ проектов</span>
        </a>
    </div>
</div>


<!--SERVICES SECTION-->
<div class="cnHeaderB" id="services"><span>Услуги:</span></div>
<div id="servSelects"><div class="servSelect active">Для частных интерьеров</div><span>|</span><div class="servSelect notActive">Для коммерческих интерьеров</div></div>
<div id="servicesSection">
    <div class="sContent">
        <div id="servicesPrivate"><!--private services-->

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Выбор квартиры/дома</div>
                <div class="servDscr">Помощь при выборе квартиры/дома, какая планировка наиболее соответствует вашим пожеланиям и даст возможность реализовать все мечты</div>
                <div class="servLine"></div>
                <div class="servPrice">400 <span>руб/м<sup>2</sup></span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Дизайн-проект</div>
                <div class="servDscr">Дизайн-проект комнат, квартир, таунхаузов, домов разной стилистики и направлений</div>
                <div class="servLine"></div>
                <div class="servPrice">4000 <span>руб/м<sup>2</sup></span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Декорирование</div>
                <div class="servDscr">Подбор предметов декора в готовый интерьер - от штор и постельного белья до сервиза и предметов искусства</div>
                <div class="servLine"></div>
                <div class="servPrice">от 600 <span>руб/м<sup>2</sup></span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Комплектация</div>
                <div class="servDscr">Просчет бюджета, подбор отделочных материалов и мебели, проверка поставок на объекте</div>
                <div class="servLine"></div>
                <div class="servPrice">от 25000 <span>руб</span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Авторский надзор</div>
                <div class="servDscr">Контроль реализации дизайн-проекта со стороны дизайнера</div>
                <div class="servLine"></div>
                <div class="servPrice">от 30 000 <span>руб/мес</span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Ремонтно-отделочные работы</div>
                <div class="servDscr">Ремонтно-отделочные работы высокого качества</div>
                <div class="servLine"></div>
                <div class="servPrice">от 16 000 <span>руб/м<sup>2</sup></span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

        </div><!--private services END-->

        <div id="servicesCommerce"><!--commerce services-->

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Айдентика</div>
                <div class="servDscr">Создание концепции объекта в целом: от оригинального названия и логотипа до оформления сайта, социальных сетей</div>
                <div class="servLine"></div>
                <div class="servPrice">от 60 000 <span>руб</span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Дизайн-проект</div>
                <div class="servDscr">Дизайн-проект комнат, квартир, таунхаузов, домов разной стилистики и направлений</div>
                <div class="servLine"></div>
                <div class="servPrice">3000 <span>руб/м<sup>2</sup></span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Декорирование</div>
                <div class="servDscr">Подбор предметов декора в готовый интерьер - от штор и постельного белья до сервиза и предметов искусства</div>
                <div class="servLine"></div>
                <div class="servPrice">от 400 <span>руб/м<sup>2</sup></span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Комплектация</div>
                <div class="servDscr">Просчет бюджета, подбор отделочных материалов и мебели, проверка поставок на объекте</div>
                <div class="servLine"></div>
                <div class="servPrice">от 25000 <span>руб</span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Авторский надзор</div>
                <div class="servDscr">Контроль реализации дизайн-проекта со стороны дизайнера</div>
                <div class="servLine"></div>
                <div class="servPrice">от 30 000 <span>руб/мес</span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

            <div class="service">
                <div class="lbCrn"></div><div class="tl"></div><div class="bl"></div><div class="ll"></div><div class="rl"></div>
                <div class="servTitle">Ремонтно-отделочные работы</div>
                <div class="servDscr">Ремонтно-отделочные работы высокого качества</div>
                <div class="servLine"></div>
                <div class="servPrice">от 14 000 <span>руб/м<sup>2</sup></span></div>
                <a href="#" class="servMore">Подробнее</a>
            </div>

        </div><!--commerce services END-->

    </div>
</div>
<!--SERVICES SECTION END-->

<div class="quiz-header">
    <p>Сэкономте свое время</p>
    <p style="font-size: 30px; text-transform: uppercase;">ВЫ МОЖЕТЕ ПРОЙТИ ТЕСТ</p>
    <p>и узнать стоимость<br>дизайна интерьера уже <span style="color: #dabf8a">через 1 минуту</span> </p>
</div>

<div class="quiz-wrapper" style="margin-bottom: 40px;">
    <div class="quiz">
        <div class="quiz-status">
            <div class="quis-status-text" style="margin-top: 40px; margin-bottom: 20px; font-size: 30px;">
                ВОПРОС <span class="questN">1</span> из 6
            </div>
            <div class="quiz-status-bar">
                <img id="quiz-status-1" src="/assets/images/quiz_status_active.png">
                <img id="quiz-status-2" src="/assets/images/quiz_status_inactive.png">
                <img id="quiz-status-3" src="/assets/images/quiz_status_inactive.png">
                <img id="quiz-status-4" src="/assets/images/quiz_status_inactive.png">
                <img id="quiz-status-5" src="/assets/images/quiz_status_inactive.png">
                <img id="quiz-status-6" src="/assets/images/quiz_status_inactive.png">
            </div>
            <div class="quiz-body">
                <div class="quiz-body-header" style="margin-top: 20px; margin-bottom: 40px; font-size: 20px;">
                    Дизайн итерьера какого объекта вас интересует?
                </div>
                <form class="quiz-form">
                    <div class="quiz-1">
                        <div class="grid" style="margin-bottom: 45px;">
                            <div class="col-1-3" style="text-align: center;">
                                <label class="img-input-wrap q1" style="margin-bottom: 3px;">
                                    <img id="q1-1" src="/assets/images/quiz-cvart.jpeg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q1-1">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Квартира</p>
                            </div>
                            <div class="col-1-3">
                                <label class="img-input-wrap q1" style="margin-bottom: 3px;"><img id="q1-2" src="/assets/images/quiz_house.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q1-2">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Дом-Котоедж-Таунхаус</p>
                            </div>
                            <div class="col-1-3">
                                <label class="img-input-wrap q1" style="margin-bottom: 3px;"><img id="q1-3" src="/assets/images/q_room.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q1-3">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Комната</p>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="col-1-3">
                                <label class="img-input-wrap q1" style="margin-bottom: 3px;"><img id="q1-4" src="/assets/images/office.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q1-4">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Офис</p>
                            </div>
                            <div class="col-1-3">
                                <label class="img-input-wrap q1" style="margin-bottom: 3px;"><img id="q1-5" src="/assets/images/cafe.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q1-5">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Ресторан-Кафе</p>
                            </div>
                            <div class="col-1-3">
                                <label class="img-input-wrap q1" style="margin-bottom: 3px;"><img id="q1-6" src="/assets/images/cafe.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q1-6">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Другое</p>
                            </div>
                        </div>
                        <a id="q-next-1" class="btn-gold btn-lg btn-theme" href="##" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 55px;">
                            <span class="btn-gold-inside" style="color: black;">Далее</span>
                        </a>
                    </div>
                    <div class="quiz-2" style="display: none;">
                        <div class="grid">
                            <div class="col-1-2">
                                <img src="/assets/images/quiz2_photo.png" style="width: 100%">
                            </div>
                            <div class="col-1-2" style="text-align: center; margin-top: 76px; margin-left: 50px;">
                                <p>Площадь вашего помещения: <span id="result"></span>м<sup>2</sup></p>
                                <div class="slidecontainer" style="margin-top: 76px;">
                                    <input type="range" min="20" max="5000" value="50" class="q-slider" id="myRange">
                                </div>
                                <div class="load-file">
                                    <p>Это не обязательно, но если есть, прикрепите планировку:</p>
                                    <input type="file" class="file-input" id="file-input">
                                    <p style="margin-top: 20px;"><span class="file-name">Название файла.png</span><a id="file-in" class="btn-gold btn-lg btn-theme" href="#" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; padding: 5px!important; padding-top: 10px!important; padding-bottom: 10px!important;">
                                            <span class="btn-gold-inside" style="color: black;">Выбрать...</span>
                                        </a></p>
                                </div>
                                <a id="q-next-2" class="btn-gold btn-lg btn-theme" href="##" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 55px;">
                                    <span class="btn-gold-inside" style="color: black;">Далее</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="quiz-3" style="display: none;">
                        <div class="grid" style="margin-bottom: 45px;">
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-1" src="/assets/images/loft.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-1">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Лофт</p>
                            </div>
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-2" src="/assets/images/modern.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-2">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Современный</p>
                            </div>
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-3" src="/assets/images/scand.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-3">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Скандинавский</p>
                            </div>
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-4" src="/assets/images/art.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-4">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Арт-Деко</p>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-5" src="/assets/images/prov.jpeg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-5">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Прованс</p>
                            </div>
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-6" src="/assets/images/neo.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-6">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Неоклассика</p>
                            </div>
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-7" src="/assets/images/class.jpeg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-7">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>Классика</p>
                            </div>
                            <div class="col-1-4">
                                <label class="img-input-wrap q2" style="margin-bottom: 3px;"><img id="q2-8" src="/assets/images/quest.jpg" style="width: 90%;" data-status="inactive">
                                    <input type="radio" name="radio" data-id="q2-8">
                                    <span class="img-checkmark"></span>
                                </label>
                                <p>В процессе выбора</p>
                            </div>
                        </div>
                        <a id="q-next-3" class="btn-gold btn-lg btn-theme" href="##" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 55px;">
                            <span class="btn-gold-inside" style="color: black;">Далее</span>
                        </a>
                    </div>
                    <div class="quiz-4" style="display: none;">
                        <div class="grid">
                            <div class="col-1-2">
                                <img src="/assets/images/q4.jpg" style="width: 100%">
                            </div>
                            <div class="col-1-2" style="margin-left: 80px; margin-top: 80px;">
                                <label class="input-wrap" id="1">Срочно, как можно быстрей
                                    <input type="radio" checked="checked" name="radio" data-id="1">
                                    <span class="checkmark"></span>
                                </label>

                                <label class="input-wrap" id="2">В спокойно режиме, есть 1-2 месяца
                                    <input type="radio" name="radio" data-id="2">
                                    <span class="checkmark"></span>
                                </label>

                                <label class="input-wrap" id="3">В течении года
                                    <input type="radio" name="radio" data-id="3">
                                    <span class="checkmark"></span>
                                </label>

                                <label class="input-wrap" id="4">Еще не определился
                                    <input type="radio" name="radio" data-id="4">
                                    <span class="checkmark"></span>
                                </label>
                                <a id="q-next-4" class="btn-gold btn-lg btn-theme" href="##" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 0px;">
                                    <span class="btn-gold-inside" style="color: black;">Далее</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="quiz-5" style="display: none;">
                        <div class="grid">
                            <div class="col-1-2">
                                <img src="/assets/images/q5.png" style="width: 100%;">
                            </div>
                            <div class="col-1-2" style="margin-left: 80px;">
                                <label id="5" class="input-wrap">Нет, нужна услуга "под ключ"
                                    <input data-id="5" type="radio" checked="checked" name="q5">
                                    <span class="checkmark"></span>
                                </label>

                                <label id="6" class="input-wrap">Иногда буду обращаться за консультацией
                                    <input data-id="6" type="radio" name="q5">
                                    <span class="checkmark"></span>
                                </label>

                                <label id="7" class="input-wrap">Есть время, знаю о ремонте все, помощь не нужна
                                    <input data-id="7" type="radio" name="q5">
                                    <span class="checkmark"></span>
                                </label>

                                <label id="8" class="input-wrap" style="margin-bottom: 15px;">Еще не определился
                                    <input data-id="8" type="radio" name="q5">
                                    <span class="checkmark"></span>
                                </label>
                                <a id="q-next-5" class="btn-gold btn-lg btn-theme" href="##" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 0px;">
                                    <span class="btn-gold-inside" style="color: black;">Далее</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="quiz-6" style="display: none;">
                        <div class="grid">
                            <div class="col-1-2" style="padding-top: 50px;">
                                <img src="/assets/images/q6.png" style="width: 100%">
                            </div>
                            <div class="col-1-2" style="width: 50%;">
                                <div class="grid" style="text-align: center;">
                                    <div class="col-1-2" style="margin-left: 20px; margin-right: 20px;">
                                        <label id="9" class="input-wrap" style=" margin-bottom: 40px;">Viber <i style="color: #930084;" class="fab fa-viber"></i>
                                            <input type="radio" name="q6" data-id="9">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label id="10" class="input-wrap" style=" padding-left: 24px; margin-bottom: 20px;">Whatsapp <i style="color: #3BC309;" class="fa fa-whatsapp"></i>
                                            <input type="radio" name="q6" data-id="10">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-1-2">
                                        <label id="11" class="input-wrap" style=" margin-bottom: 40px;">Telegram <i style="color:#4684EE;" class="fa fa-telegram"></i>
                                            <input type="radio" name="q6" data-id="11">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label id="12" class="input-wrap" style="margin-bottom: 20px;">На почту <i class="fas fa-envelope" style="color: #dabf8a;"></i>
                                            <input type="radio" name="q6" data-id="12">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <p>Введите ваши данные, и мы вышлем бонусы и просчет на указанный способ в течении 5 минут! </p>
                                <div id="qSampleForm" style="padding-top: 20px">
                                    <input id="af_name" class="form-control" type="text" name="name" value="" placeholder="Ваше имя*" style="margin-bottom: 10px;">
                                    <!--                <span class="error_name"></span>-->
                                    <input type="text" value="" placeholder="Телефон для связи*">
                                </div>
                                <div class="form-group">
                                    <div class="controls" style="padding-bottom: 0px;">
                                        <a id="q-next-6" class="btn-gold btn-lg btn-theme" href="#" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 10px; margin-bottom: 30px; color: black;">
                                            <span style="font-size: 15px!important;" class="btn-gold-inside">Получить результат</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="quiz-side">
        <img src="/assets/images/quiz_side_photo.png">
        <p>
            Здравствуйте!<br>
            Меня зовут Александра, руководитель студии А8.<br><br><br>
        </p>
        <p>
            Благодоря данным ответам, мы сможем сэкономить ваше время и предоставить сразу стоимость дизайн проекта, а так же сооринтировать по дополнительным слугам
        </p>
        <p style="padding-top: 30px; padding-bottom: 10px; font-size: 25px; font-weight: bold">
            В конце теста вы получте:
        </p>
        <div class="grid">
            <div class="col-1-3">
                <img src="/assets/images/quiz_side_ico1.png" style="width: 100%">
            </div>
            <div class="col-2-3">
                <p>Предложение стоимости и срока вашего проекта</p>
            </div>
        </div>
        <div class="grid">
            <div class="col-1-3">
                <img src="/assets/images/quiz_side_ico2.png" style="width: 100%">
            </div>
            <div class="col-2-3">
                <p>Каталог ТОП проектов от нашей студии PDF файл</p>
            </div>
        </div>
    </div>
</div>

<div class="thanx" style="display: none;">
    <div class="thanx-header">
        <p>
            Благодарим вас <br>
            за доверие
        </p>
    </div>
    <div id="lineDecor">
    </div>
    <p style="margin-left: 150px; margin-right: 150px;">
        Пока ваш запрос обрабатывается, предлагаем подписаться на наш инстаграмм, что бы всегда быть в курсе новых идей и рещений в сфере дизайна
    </p>
    <div class="grid"><div class="col-1-2">
        <img src="/assets/images/tnx.png" style="width: 100%">
    </div>
    <div class="col-1-2">
        <a id="q-next-6" class="btn-gold btn-lg btn-theme" href="#" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 200px; margin-left: 30px; color: black;">
            <span style="font-size: 15px!important;" class="btn-gold-inside">Instagramm</span>
        </a>
    </div>
    </div></div>

<div id="personSection" style="margin-bottom: 0;">
    <div class="sContent">
        <div class="tourVLines">
            <div id="personContent">
                <div id="personPic"><img src="/assets/images/person_pic.png"></div>
                <div id="personInfo">
                    <div id="pName">Александра Булатова</div>
                    <div id="pDscr">Архитектор, выпускница Академии Художеств им. И. Е. Репина,<br>руководитель дизайн-студии «Архитектура Восьмого Дня»</div>
                    <div id="pQuote"><div id="quotes"></div><p>Дизайн интерьера - это не просто красивые вещи, правильно расставленная мебель и эргоэкономика пространства.<br>Для меня – это жизнь, жизнь людей в данном помещении.<br>И главной целью становится сделать эту жизнь комфортной, красивой и доступной каждому человеку.</p><img src="/assets/images/signif2.png" id="sign"></div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="consult" style="margin-top: 0;">
    <div class="sContent">
        <p style="font-size: 30px; text-transform: uppercase; padding-top: 50px; padding-bottom: 23px; color: white;">
            Получите консультацию<br>главного архитектора
        </p>
        <form class="ajax_form af_example" action="" method="post">
            <div id="projectSampleForm">
                <span style="color: white;"><br>Введите ваше имя<br></span>
                <input id="af_name" class="form-control" type="text" name="name" value="" placeholder="Ваше имя" style="margin-bottom: 30px;">
<!--                <span class="error_name"></span>-->
                <span style="color: white;"><br>Введите Ваш телефон<br></span>
                <input type="text" value="+7 (999) 999 99 99">
            </div>
            <div class="form-group">
                <div class="controls" style="padding-bottom: 20px;">
                    <a class="btn-gold btn-lg btn-theme" href="#" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 55px;">
                        <span class="btn-gold-inside">Получить Консультацию</span>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="cnHeaderB"><span>Почему мы:</span></div>
<div id="causesSection">
    <div class="sContent">
        <div class="luCrn"></div>
        <div class="rbCrn"></div>

        <table id="causes">
            <tr>
                <td style="padding-bottom: 10px;"><div class="cPic"><img src="/assets/images/cause2.png"></div><div class="cText">С нами лкгко начать!<br>Сделаем быстро 3 варианта планировок за 3 дня, и отправим вам с видео пояснением от руководителя студии.</div></div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Узнать больше</button></td>
                <td style="padding-bottom: 10px;"><div class="cPic"><img src="/assets/images/cause2.png"></div><div class="cText">Опыт в проектировании и реализации объектов различной сложности, стилистики и задач уже более 9 лет!</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Смотреть портфолио</button></td>
                <td style="padding-bottom: 10px;"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Самый полный и подробный комплект чертежей со всеми уточнениями, пометками и разработками от демонтажа стен до мебели и специальных изделий</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Видео с пояснением содержания проекта</button></td>
            </tr>
            <tr>
                <td style="padding-bottom: 10px;"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Виртуальный 3D тур по вашему объекту.<br>Плоские картинки никогда не передадут полного ощущения от объема и масштаба интерьера.<br>Побывайте внутри мечты уже сегодня!</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Посмотреть пример 3D тура</button></td>
                <td style="padding-bottom: 10px;"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Посчитаем точный бюджет до начала реализации проекта от ремонта до светильников, мебели. Проконтролируем все доставки и установки на объекте.</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Подробнее об услуге</button></td>
                <td style="padding-bottom: 10px; border-bottom: 0; border-right: 0;"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Реализация проекта "под ключ".<br>Нервы, проблемы и тысячи вопросов оставьте нам. Уделите время семье и отдыху, а мы займемся ремонтом для вас.</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Подробнее об услуге</button></td>
            </tr>
        </table>

        <div id="causesMobile">
            <div class="causeMob"><div class="cPic"><img src="/assets/images/cause1.png"></div><div class="cText">С нами лкгко начать!<br>Сделаем быстро 3 варианта планировок за 3 дня, и отправим вам с видео пояснением от руководителя студии.</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Узнать больше</button></div>
            <div class="causeMob"><div class="cPic"><img src="/assets/images/cause2.png"></div><div class="cText">Опыт в проектировании и реализации объектов различной сложности, стилистики и задач уже более 9 лет!</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Смотреть портфолио</button></div>
            <div class="causeMob"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Самый полный и подробный комплект чертежей со всеми уточнениями, пометками и разработками от демонтажа стен до мебели и специальных изделий</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Видео с пояснением содержания проекта</button></div>
            <div class="causeMob"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Виртуальный 3D тур по вашему объекту.<br>Плоские картинки никогда не передадут полного ощущения от объема и масштаба интерьера.<br>Побывайте внутри мечты уже сегодня!</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Посмотреть пример 3D тура</button></div>
            <div class="causeMob"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Посчитаем точный бюджет до начала реализации проекта от ремонта до светильников, мебели. Проконтролируем все доставки и установки на объекте.</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Подробнее об услуге</button></div>
            <div class="causeMob"><div class="cPic"><img src="/assets/images/cause3.png"></div><div class="cText">Реализация проекта "под ключ".<br>Нервы, проблемы и тысячи вопросов оставьте нам. Уделите время семье и отдыху, а мы займемся ремонтом для вас.</div><button id="sH" class="btn btn-primary" style="padding-bottom: 10px; padding-top: 10px; width: 100%; margin-top: 10px;">Подробнее об услуге</button></div>
        </div>

    </div>
</div>

<div class="lineHeader">ЗА ЧТО НАМ ГОВОРЯТ СПАСИБО!</div>
<div class="slider" style="width: 80%;">
    <div>
        <div class="slide-h3">
            <i class="fa fa-lg fa-trash"></i>
            <h3>
                1
            </h3></div>
    </div>
    <div>
        <div class="slide-h3">
            <i class="fa fa-lg fa-trash"></i>
            <h3>
                2
            </h3></div>
    </div>
    <div>
        <div class="slide-h3">
            <i class="fa fa-lg fa-trash"></i>
            <h3>
                3
            </h3></div>
    </div>
    <div>
        <div class="slide-h3">
            <i class="fa fa-lg fa-trash"></i>
            <h3>
                4
            </h3></div>
    </div>
    <div>
        <div class="slide-h3">
            <i class="fa fa-lg fa-trash"></i>
            <h3>
                5
            </h3></div>
    </div>
    <div>
        <div class="slide-h3">
            <i class="fa fa-lg fa-trash"></i>
            <h3>6</h3>
        </div>
    </div>
</div>

<div class="lineHeader">НАМ ДОВЕРЯЮТ</div>
<div class="sponsor">
    <p>
        <img src="/assets/images/logo_s_1.png">
        <img src="/assets/images/logo_s_2.png">
        <img src="/assets/images/logo_s_3.png">
        <img src="/assets/images/logo_s_4.png">
        <img src="/assets/images/logo_s_5.png">
        <img src="/assets/images/logo_s_6.png">
        <img src="/assets/images/logo_s_7.png">
        <img src="/assets/images/logo_s_8.png">
        <img src="/assets/images/logo_s_9.png">
        <img src="/assets/images/logo_s_10.png">
        <img src="/assets/images/logo_s_11.png">
        <img src="/assets/images/logo_s_12.png">
        <img src="/assets/images/logo_s_13.png">
        <img src="/assets/images/logo_s_14.png">
        <img src="/assets/images/logo_s_15.png">
    </p>
</div>



<div class="bigHeader procBH">Как формируется процесс работы</div>
<div id="processSection">
    <div class="sContent">
        <div class="luCrn"></div>
        <div class="rbCrn"></div>
        <div id="processBlc">
            <div id="pBFrame"><div></div></div>

            <!--
            <div id="procPic"><img src="assets/images/proc01.jpg"><div id="pPicOvr"><div id="pFrame"><div id="pNumb">1</div></div></div></div>
            <div id="procTitle">Обращение в нашу студию</div>
            <div id="procText"><p>Свяжитесь с нами любым удобным способом, и вы получите развернутую информацию о том, как строится работа над дизайн-проектом, из чего складывается его стоимость, а также о сроках его разработки и последующей реализации.</p></div>-->

            <div id="processSlides">
                <div class="psSlide"><img src="/assets/images/proc01.jpg"><span class="pSTitle">Обращение в нашу студию</span><span class="pSDscr"><p>Свяжитесь с нами любым удобным способом, и вы получите развернутую информацию о том, как строится работа над дизайн-проектом, из чего складывается его стоимость, а также о сроках его разработки и последующей реализации.</p></span></div>
                <div class="psSlide"><img src="/assets/images/proc02.jpg"><span class="pSTitle">Заголовок 2</span><span class="pSDscr"><p>Текст 2</p></span></div>
                <div class="psSlide"><img src="/assets/images/proc03.jpg"><span class="pSTitle">Заголовок 3</span><span class="pSDscr"><p>Текст 3</p></span></div>
                <div class="psSlide"><img src="/assets/images/proc01.jpg"><span class="pSTitle">Заголовок 4</span><span class="pSDscr"><p>Текст 4</p></span></div>
                <div class="psSlide"><img src="/assets/images/proc02.jpg"><span class="pSTitle">Заголовок 5</span><span class="pSDscr"><p>Текст 5</p></span></div>
            </div>

            <div id="pPager">
                <span id="prev"><img src="/assets/images/left.png"><a>Готовый объект</a><span>12</span></span>
                <span id="current">1</span>
                <span id="next"><span>2</span><a>Заключение договора</a><img src="/assets/images/right.png"></span>
            </div>
        </div>
    </div>
</div>

<!--<div id="toLPsSection">-->
<!--    <div id="toLPs">-->
<!--        <div class="toLP"><img src="/images/prj2.jpg"><div class="toLPOvr"><div class="toLPFrame"><div class="toLpTitle">Авторский надзор<span class="lbCrn"></span></div><div class="toLPDscr">Контроль дизайнера за ходом выполнения ремонтно-строительных работ</div><a href="#" class="toLPLink">Узнать больше</a></div></div></div>-->
<!--        <div class="toLP"><img src="/images/prj1.jpg"><div class="toLPOvr"><div class="toLPFrame"><div class="toLpTitle">Генеральный подряд<span class="lbCrn"></span></div><div class="toLPDscr">Все, что вам нужно сделать - отдать нам ключи и готовиться к новоселью в согласованный срок сдачи</div><a href="#" class="toLPLink">Узнать больше</a></div></div></div>-->
<!--        <div class="toLP"><img src="/images/prj3.jpg"><div class="toLPOvr"><div class="toLPFrame"><div class="toLpTitle">Ремонтно-отделочные работы<span class="lbCrn"></span></div><div class="toLPDscr">Ремонтно-отделочные работы высокого качества</div><a href="#" class="toLPLink">Узнать больше</a></div></div></div>-->
<!--    </div>-->
<!--</div>-->

<div id="personSection" style="margin-bottom: 0; margin-top: 20px;">
    <div class="sContent">
        <div class="tourVLines">
            <div id="personContent">
                <div id="personPic"><img src="/assets/images/person_pic.png"></div>
                <div id="personInfo">
                    <div class="contact-header">КОНТАКТЫ:</div>
                    <div class="contacts-body">
                        <div class="footerContact"><img src="/assets/images/phone.png"><span>8 (812) 925 66 52</span></div>
                        <div class="footerContact"><img src="/assets/images/mail.png"><span>info@designa8.ru</span></div>
                        <div class="footerContact"><img src="/assets/images/clock_icon.png"><span>с 10 до 19 пн-пт</span></div>
                    </div>
                    <div class="contacts-buttons">
                        <p>Задайте любой вопрос:<br></p>
                        <p style="padding-top: 20px;">
                            <button class="btn-contacts">Viber <i style="color: #930084;" class="fab fa-viber"></i></button>
                            <button class="btn-contacts">Whatsapp <i style="color: #3bc309;" class="fa fa-whatsapp"></i></button>
                            <button class="btn-contacts">Telegram <i style="color: #4684ee;" class="fa fa-telegram"></i></button>
                        </p>
                    </div>
                    <div class="socials">
                        <p>Мы в социальных сетях</p>
                        <div id="footerSocial">
                            <a href="#"><img src="/assets/images/footSocFb.png"></a>
                            <a href="#"><img src="/assets/images/footSocIg.png"></a>
                            <a href="#"><img src="/assets/images/footSocVk.png"></a>
                        </div>
                        <a class="btn-gold btn-lg btn-theme" href="#" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); text-decoration: none; margin-top: 15px;">
                            <span class="btn-gold-inside">Получить консультацию</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="map"><img src="/assets/images/map.png" style="margin-top: 0;"></div>

<div id="footerWrap">
    <div id="footer" style="padding-top: 30px;">





        <div id="footerMenu" style="border-top: 1px solid #dabf8a">
            <div class="menuCol"><a href="#" class="mainLink">Главная</a></div>
            <div class="menuCol"><a href="#" class="mainLink">О компании</a><a href="#">Наша история</a><a href="#">Команда</a><a href="#">Миссия</a><a href="#">Офис</a><a href="#">Вакансии</a><a href="#">Реквизиты</a></div>
            <div class="menuCol"><a href="#" class="mainLink">Дизайн интерьера</a><a href="#">Квартиры</a><a href="#">Комнаты</a><a href="#">Дома</a><a href="#">Стили интерьера</a><a href="#">Офисы</a></div>
            <div class="menuCol"><a href="#" class="mainLink">Проектирование</a><a href="#">Архитектура</a><a href="#">Ландшафт</a></div>
            <div class="menuCol"><a href="#" class="mainLink">Отделка</a><a href="#">Коттеджи</a><a href="#">Квартиры</a></div>
            <div class="menuCol"><a href="#" class="mainLink">Портфолио</a><a href="#">Квартиры</a><a href="#">Дома</a><a href="#">Коммерческий дизайн</a></div>
            <div class="menuCol"><a href="#" class="mainLink">Контакты</a></div>
            <div class="menuCol"><div id="copy">
                    <p>&copy;&nbsp;ООО&nbsp;&laquo;Архитектура восьмого дня&raquo;</p>
                    <p>Все права защищены. Любое использование, либо копирование материалов или подборки материалов сайта, элементов дизайнаи оформления допускается лишь с разрешения и только со ссылкой на источник: https://designa8.ru</p>
                    <p>Заполняя любую форму на сайте, Вы соглашаетесь с <a href="#">политикой конфиденциальности</a></p>
                    <p><b>Наши реквизиты:</b></p>
                    <p>ИНН: 7805654337 / КПП: 780501001<br>ОГРН: 1147847232455</p>
                </div>
            </div>
        </div>

    </div>
</div>

<a href="#" id="popup__toggle" onclick="return false;"><div class="img-circle" style="transform-origin: center;"><div class="img-circleblock" style="transform-origin: center;"></div></div></a>
<a href="/#" class="scroll-top" style="display: none;"><i class="fa fa-chevron-up"></i></a>

<script>

    $(\'.q1 input\').change(function () {
        let id = $(this).data(\'id\');
        for (let i = 1; i <=6; i++) {
            $("#q1-" + i).css(\'opacity\', \'0.5\');
        }
        $(\'#\' + id).css(\'opacity\', \'1\');
    });

    $(\'.q2 input\').change(function () {
        let id = $(this).data(\'id\');
        for (let i = 1; i <= 8; i++) {
            $(\'#q2-\' + i).css(\'opacity\', \'0.5\');
        }
        $(\'#\' + id).css(\'opacity\', \'1\');
    });

    $(\'.input-wrap input\').change(function () {
        $(\'.input-wrap\').css(\'border\', \'none\');
       let id = $(this).data(\'id\');
       if ($(this).prop(\'checked\')) {
           $(\'#\' + id).css(\'border\', \'1px solid #dabf8a\');
       }
    });

    $(\'.file-in\').click(function () {
       $(\'#file-input\').click();
       $(\'.file-name\').html($(\'#file-input\').value);
    });

    let slider = document.getElementById(\'myRange\');
    let output = document.getElementById(\'result\');
    output.innerHTML = slider.value;

    slider.oninput = function() {
      output.innerHTML = this.value;
    };

    $(\'#q-next-1\').click(function () {
        $(\'.questN\').html(\'2\');
        $(\'#quiz-status-1\').attr(\'src\', \'/assets/images/quiz_status_inactive.png\');
        $(\'#quiz-status-2\').attr(\'src\', \'/assets/images/quiz_status_active.png\');
        $(\'.quiz-body-header\').html(\'Укажите общую площадь\');
        $(\'.quiz-1\').css(\'display\', \'none\');
        $(\'.quiz-2\').css(\'display\', \'block\');
    });

    $(\'#q-next-2\').click(function () {
        $(\'.questN\').html(\'3\');
        $(\'#quiz-status-2\').attr(\'src\', \'/assets/images/quiz_status_inactive.png\');
        $(\'#quiz-status-3\').attr(\'src\', \'/assets/images/quiz_status_active.png\');
        $(\'.quiz-body-header\').html(\'Выберите стили, которые вас интересуют\');
        $(\'.quiz-2\').css(\'display\', \'none\');
        $(\'.quiz-3\').css(\'display\', \'block\');
    });

    $(\'#q-next-3\').click(function () {
        $(\'.questN\').html(\'4\');
        $(\'#quiz-status-3\').attr(\'src\', \'/assets/images/quiz_status_inactive.png\');
        $(\'#quiz-status-4\').attr(\'src\', \'/assets/images/quiz_status_active.png\');
        $(\'.quiz-body-header\').html(\'Когда вы хотите начать работы?\');
        $(\'.quiz-3\').css(\'display\', \'none\');
        $(\'.quiz-4\').css(\'display\', \'block\');
    });

    $(\'#q-next-4\').click(function () {
        $(\'.questN\').html(\'5\');
        $(\'#quiz-status-4\').attr(\'src\', \'/assets/images/quiz_status_inactive.png\');
        $(\'#quiz-status-5\').attr(\'src\', \'/assets/images/quiz_status_active.png\');
        $(\'.quiz-body-header\').html(\'У вас есть время и желание учавствовать в процессе ремнта\');
        $(\'.quiz-4\').css(\'display\', \'none\');
        $(\'.quiz-5\').css(\'display\', \'block\');
    });

    $(\'#q-next-5\').click(function () {
        $(\'.quiz-status-text\').html(\'Готово!\');
        $(\'#quiz-status-5\').attr(\'src\', \'/assets/images/quiz_status_inactive.png\');
        $(\'#quiz-status-6\').attr(\'src\', \'/assets/images/quiz_status_active.png\');
        $(\'.quiz-body-header\').html(\'Как вам удобней получить результат и бонусы?\');
        $(\'.quiz-5\').css(\'display\', \'none\');
        $(\'.quiz-6\').css(\'display\', \'block\');
    });

    $(\'#q-next-6\').click(function () {
        $(\'.quiz-wrapper\').css(\'display\', \'none\');
        $(\'.thanx\').css(\'display\', \'block\');
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $(\'.scroll-top\').fadeIn();
        } else {
            $(\'.scroll-top\').fadeOut();
        }
    });

    $(\'.scroll-top\').click(function () {
       $("html, body").animate({
           scrollTop: 0
       }, 600);
       return false;
    });


    $(\'.mouse\').click(function () {
        $(\'html\').animate({
            scrollTop: 750
        }, 500);
        return false;
    });

    //compare function
    compareInit=function(){
        jQuery(\'#compareB\').html(\'<img class="changing" src="\'+jQuery(\'.cmpPr.current img\').eq(1).attr(\'src\')+\'">\');
        jQuery(\'#compareA img\').remove();
        jQuery(\'#compareA\').prepend(\'<img class="changing" src="\'+jQuery(\'.cmpPr.current img\').eq(0).attr(\'src\')+\'">\')
        jQuery(\'#rcPager span.active\').removeClass(\'active\');
        jQuery(\'#rcPager span\').eq(jQuery(\'.cmpPr.current\').index()).addClass(\'active\');
        setTimeout(function(){
            jQuery(\'#compareA img, #compareB img\').removeClass(\'changing\');
        }, 200);

        jQuery("#handle" ).draggable({ axis: "x", containment: "#compareViewport", scroll: false });
    };

    planingSlide=function(){
        pslTitle=jQuery(\'.pslSlide.current .pslTitle\').text();
        pslImg=jQuery(\'.pslSlide.current img\').attr(\'src\');
        pslCapt=jQuery(\'.pslSlide.current .pslCapt\').text();
        jQuery(\'#pslView\').html(\'<div id="pslTitle">\'+pslTitle+\'</div><div id="pslPic"><img src="\'+pslImg+\'"></div><div id="pslCapt">\'+pslCapt+\'</div>\');
        jQuery(\'#pslPager span.active\').removeClass(\'active\');
        jQuery(\'#pslPager span\').eq(jQuery(\'.pslSlide.current\').index()).addClass(\'active\');

    };
    jQuery(document).ready(function(){

        if(window.location.hash!=\'\'){
            scrollID=window.location.hash;
            scrollPos=jQuery(scrollID).offset().top-80;
            jQuery("html, body").scrollTop(scrollPos)
        }

        $(\'.quiz-slider\').slider({
            animate: true,
            range: \'min\',
            value: 0,
            min: 20,
            max: 5000,
            step: 1,
            slide: function(event, ui) {
                $(\'#slider-result\').html(ui.value);
            },
            change: function(event, ui) {
                $(\'#znch\').attr(\'value\', ui.value);
            }
        });

        $(\'.slider\').slick({
            centerMode: true,
            centerPadding: \'60px\',
            slidesToShow: 3,
            speed: 1500,
            index: 2,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: \'40px\',
                    slidesToShow: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: \'40px\',
                    slidesToShow: 1
                }
            }]
        });

        //compare initialize
        if(jQuery(\'#compareViewport\').length){
            for(i=1; i<=jQuery(\'.cmpPr\').length; i++){
                jQuery(\'#rcPager\').prepend(\'<span></span>\')
            }
            jQuery(\'.cmpPr\').first().addClass(\'current\');


            compareInit();
        }

        //planing slider load
        if(jQuery(\'#pslSlider\').length){
            for(i=1; i<=jQuery(\'.pslSlide\').length; i++){
                jQuery(\'#pslPager\').prepend(\'<span></span>\')
            }
            jQuery(\'#pslAllSlides .pslSlide\').first().addClass(\'current\');
            planingSlide();
        }

        //processSlider load
        totalPsSlides=jQuery(\'.psSlide\').length;
        jQuery(\'#processSlides .psSlide\').first().addClass(\'current\');
        jQuery(\'#processSlides .psSlide\').eq(1).addClass(\'psNext\');
        jQuery(\'#processSlides .psSlide\').last().addClass(\'psPrev\');
        currentSlide=jQuery(\'.psSlide.current\');


        psImgSrc=currentSlide.children(\'img\').attr(\'src\');
        psTitle=currentSlide.children(\'.pSTitle\').text();
        psDscrHtml=currentSlide.children(\'.pSDscr\').html();

        psNextTitle=jQuery(\'.psSlide.psNext\').children(\'.pSTitle\').text()
        psPrevTitle=jQuery(\'.psSlide.psPrev\').children(\'.pSTitle\').text();


        jQuery(\'#pPager #next a\').text(psNextTitle);
        jQuery(\'#pPager #next span\').text(2);
        jQuery(\'#pPager #prev a\').text(psPrevTitle);
        jQuery(\'#pPager #prev span\').text(totalPsSlides);


        jQuery(\'#pBFrame\').after(\'<div id="procPic"><img src="\'+psImgSrc+\'"><div id="pPicOvr"><div id="pFrame"><div id="pNumb">1</div></div></div></div><div id="procTitle">\'+psTitle+\'</div><div id="procText">\'+psDscrHtml+\'</div>\');

        //pie diagramm load
        if(jQuery(\'#diagramm\').length){
            jQuery(\'.percent\').each(function(){
                bgCol=jQuery(\'.percentColor\',this).css(\'background-color\');
                jQuery(\'.chart\').eq(jQuery(this).index()).attr(\'fill\',bgCol)
            })
        }

    });

    //compare actions
    //handle
    jQuery( "#handle" ).on( "drag", function( event, ui ) {
        x=ui.position.left;
        jQuery(\'#compareA\').width(x);
    });
    //pager
    jQuery(\'body\').on(\'click\', \'#rcPager span\', function(){
        jQuery(\'#rcPager span.active\').removeClass(\'active\');
        jQuery(this).addClass(\'active\');
        jQuery(\'.cmpPr.current\').removeClass(\'current\');
        jQuery(\'.cmpPr\').eq(jQuery(this).index()).addClass(\'current\');
        jQuery(\'#compareA img, #compareB img\').addClass(\'changing\');
        setTimeout(function(){compareInit()}, 200);
    });
    //next
    jQuery(\'body\').on(\'click\', \'#rcNext\', function(){
        if(jQuery(\'.cmpPr.current\').is(\'.cmpPr:last\')){
            jQuery(\'.cmpPr.current\').removeClass(\'current\');
            jQuery(\'.cmpPr\').first().addClass(\'current\');
        }
        else{
            jQuery(\'.cmpPr.current\').removeClass(\'current\').next(\'.cmpPr\').addClass(\'current\');
        }
        jQuery(\'#compareA img, #compareB img\').addClass(\'changing\');
        setTimeout(function(){compareInit()}, 200);
    });
    //prev
    jQuery(\'body\').on(\'click\', \'#rcPrev\', function(){
        if(jQuery(\'.cmpPr.current\').is(\'.cmpPr:first\')){
            jQuery(\'.cmpPr.current\').removeClass(\'current\');
            jQuery(\'.cmpPr\').last().addClass(\'current\');
        }
        else{
            jQuery(\'.cmpPr.current\').removeClass(\'current\').prev(\'.cmpPr\').addClass(\'current\');
        }
        jQuery(\'#compareA img, #compareB img\').addClass(\'changing\');
        setTimeout(function(){compareInit()}, 200);
    });

    //pie diagramm actions
    jQuery(\'body\').on(\'mouseenter\', \'.chart\', function(){
        jQuery(\'.chart\').addClass(\'unhovered\');
        jQuery(this).addClass(\'hovered\');
        jQuery(\'.percent\').eq(jQuery(this).index()).addClass(\'hovered\')

    });
    jQuery(\'body\').on(\'mouseleave\', \'.chart\', function(){
        jQuery(\'.chart\').removeClass(\'unhovered\');
        jQuery(this).removeClass(\'hovered\');
        jQuery(\'.percent\').eq(jQuery(this).index()).removeClass(\'hovered\')
    });
    jQuery(\'body\').on(\'mouseenter\', \'.percent\', function(){
        jQuery(this).addClass(\'hovered\');
        jQuery(\'.chart\').addClass(\'unhovered\');
        jQuery(\'.chart\').eq(jQuery(this).index()).addClass(\'hovered\')

    });
    jQuery(\'body\').on(\'mouseleave\', \'.percent\', function(){
        jQuery(this).removeClass(\'hovered\');
        jQuery(\'.chart\').removeClass(\'unhovered\');
        jQuery(\'.chart\').eq(jQuery(this).index()).removeClass(\'hovered\')
    });

    //processSlider actions
    //next
    jQuery(\'body\').on(\'click\', \'#pPager #next\', function(){
        if(jQuery(\'.psSlide.current\').is(\'.psSlide:last\')){
            jQuery(\'.psSlide.current\').removeClass(\'current\');
            jQuery(\'.psSlide\').first().addClass(\'current\');

        }
        else{
            jQuery(\'.psSlide.current\').removeClass(\'current\').next(\'.psSlide\').addClass(\'current\');
        }

        if(jQuery(\'.psSlide.psNext\').is(\'.psSlide:last\')){
            jQuery(\'.psSlide.psNext\').removeClass(\'psNext\')
            jQuery(\'.psSlide\').first().addClass(\'psNext\');
        }
        else{
            jQuery(\'.psSlide.psNext\').removeClass(\'psNext\').next(\'.psSlide\').addClass(\'psNext\');
        }

        if(jQuery(\'.psSlide.psPrev\').is(\'.psSlide:last\')){
            jQuery(\'.psSlide.psPrev\').removeClass(\'psPrev\')
            jQuery(\'.psSlide\').first().addClass(\'psPrev\');
        }
        else{
            jQuery(\'.psSlide.psPrev\').removeClass(\'psPrev\').next(\'.psSlide\').addClass(\'psPrev\');
        }

        currentSlide=jQuery(\'.psSlide.current\');
        psImgSrc=currentSlide.children(\'img\').attr(\'src\');
        psTitle=currentSlide.children(\'.pSTitle\').text();
        psDscrHtml=currentSlide.children(\'.pSDscr\').html();
        psNextTitle=jQuery(\'.psSlide.psNext\').children(\'.pSTitle\').text()
        psPrevTitle=jQuery(\'.psSlide.psPrev\').children(\'.pSTitle\').text();


        jQuery(\'#procText, #procPic, #procTitle\').addClass(\'changing\')
        setTimeout(function(){
            jQuery(\'#procText\').html(psDscrHtml).removeClass(\'changing\')
            jQuery(\'#procTitle\').text(psTitle).removeClass(\'changing\')
            jQuery(\'#procPic img\').attr(\'src\', psImgSrc);
            jQuery(\'#procPic\').removeClass(\'changing\')
            jQuery(\'#pPager #next a\').text(psNextTitle);
            jQuery(\'#pPager #prev a\').text(psPrevTitle);
            jQuery(\'#pPager #current, #pNumb\').text(jQuery(\'.psSlide.current\').index()+1);
            jQuery(\'#pPager #next span\').text(jQuery(\'.psSlide.psNext\').index()+1);
            jQuery(\'#pPager #prev span\').text(jQuery(\'.psSlide.psPrev\').index()+1);
        }, 500);
    });
    //prev
    jQuery(\'body\').on(\'click\', \'#pPager #prev\', function(){
        if(jQuery(\'.psSlide.current\').is(\'.psSlide:first\')){
            jQuery(\'.psSlide.current\').removeClass(\'current\');
            jQuery(\'.psSlide\').last().addClass(\'current\');

        }
        else{
            jQuery(\'.psSlide.current\').removeClass(\'current\').prev(\'.psSlide\').addClass(\'current\');
        }

        if(jQuery(\'.psSlide.psNext\').is(\'.psSlide:first\')){
            jQuery(\'.psSlide.psNext\').removeClass(\'psNext\')
            jQuery(\'.psSlide\').last().addClass(\'psNext\');
        }
        else{
            jQuery(\'.psSlide.psNext\').removeClass(\'psNext\').prev(\'.psSlide\').addClass(\'psNext\');
        }

        if(jQuery(\'.psSlide.psPrev\').is(\'.psSlide:first\')){
            jQuery(\'.psSlide.psPrev\').removeClass(\'psPrev\')
            jQuery(\'.psSlide\').last().addClass(\'psPrev\');
        }
        else{
            jQuery(\'.psSlide.psPrev\').removeClass(\'psPrev\').prev(\'.psSlide\').addClass(\'psPrev\');
        }

        currentSlide=jQuery(\'.psSlide.current\');
        psImgSrc=currentSlide.children(\'img\').attr(\'src\');
        psTitle=currentSlide.children(\'.pSTitle\').text();
        psDscrHtml=currentSlide.children(\'.pSDscr\').html();
        psNextTitle=jQuery(\'.psSlide.psNext\').children(\'.pSTitle\').text()
        psPrevTitle=jQuery(\'.psSlide.psPrev\').children(\'.pSTitle\').text();


        jQuery(\'#procText, #procPic, #procTitle\').addClass(\'changing\')
        setTimeout(function(){
            jQuery(\'#procText\').html(psDscrHtml).removeClass(\'changing\')
            jQuery(\'#procTitle\').text(psTitle).removeClass(\'changing\')
            jQuery(\'#procPic img\').attr(\'src\', psImgSrc);
            jQuery(\'#procPic\').removeClass(\'changing\')
            jQuery(\'#pPager #next a\').text(psNextTitle);
            jQuery(\'#pPager #prev a\').text(psPrevTitle);
            jQuery(\'#pPager #current, #pNumb\').text(jQuery(\'.psSlide.current\').index()+1);
            jQuery(\'#pPager #next span\').text(jQuery(\'.psSlide.psNext\').index()+1);
            jQuery(\'#pPager #prev span\').text(jQuery(\'.psSlide.psPrev\').index()+1);
        }, 500);

    });

    //planing slider actions
    //next
    jQuery(\'body\').on(\'click\', \'#pslNext\', function(){
        if(jQuery(\'.pslSlide.current\').is(\'.pslSlide:last\')){
            jQuery(\'.pslSlide.current\').removeClass(\'current\');
            jQuery(\'.pslSlide\').first().addClass(\'current\');
        }
        else{
            jQuery(\'.pslSlide.current\').removeClass(\'current\').next(\'.pslSlide\').addClass(\'current\');
        }
        jQuery(\'#pslPic\').addClass(\'changing\');
        setTimeout(function(){planingSlide()}, 300);

    });
    //prev
    jQuery(\'body\').on(\'click\', \'#pslPrev\', function(){
        if(jQuery(\'.pslSlide.current\').is(\'.pslSlide:first\')){
            jQuery(\'.pslSlide.current\').removeClass(\'current\');
            jQuery(\'.pslSlide\').last().addClass(\'current\');
        }
        else{
            jQuery(\'.pslSlide.current\').removeClass(\'current\').prev(\'.pslSlide\').addClass(\'current\');
        }
        jQuery(\'#pslPic\').addClass(\'changing\');
        setTimeout(function(){planingSlide()}, 300);
    });
    //pager
    jQuery(\'body\').on(\'click\', \'#pslPager span\', function(){
        jQuery(\'#pslPager span.active\').removeClass(\'active\');
        jQuery(this).addClass(\'active\');
        jQuery(\'.pslSlide.current\').removeClass(\'current\');
        jQuery(\'.pslSlide\').eq(jQuery(this).index()).addClass(\'current\');
        jQuery(\'#pslPic\').addClass(\'changing\');
        setTimeout(function(){planingSlide()}, 300);
    });

    jQuery(\'body\').on(\'click\', \'#mobMenuBtn\', function(){
        jQuery(\'#mobileMenu\').slideDown()
    });
    jQuery(\'body\').on(\'click\', \'#mobileMenu a, #mobileMenuClose\', function(){
        jQuery(\'#mobileMenu\').slideUp()
    });

    //anchored links scrolling
    jQuery(\'body\').on(\'click\', \'#header a\', function(e){
        if(jQuery(this).attr(\'href\').charAt(1)==\'#\' && window.location.pathname=="/")
        {

            e.preventDefault();
            scrollID=jQuery(this).attr(\'href\').slice(1);
            scrollPos=jQuery(scrollID).offset().top-80;
            jQuery("html, body").animate({scrollTop:scrollPos},800)
        }
    });
    //anchored links scrolling END


    jQuery(\'body\').on(\'click\', \'#sH\', function(){
        jQuery(\'#sampleFormDscr, #projectSampleForm, #sH\').hide(0);
        jQuery(\'#sampleFormAfter\').show(0);
    });

    jQuery(\'body\').on(\'click\', \'.servSelect.notActive\', function(){
        jQuery(\'.servSelect\').toggleClass(\'active notActive\');
        jQuery(\'#servicesPrivate, #servicesCommerce\').fadeToggle(200);

    });
</script>

</body>
</html>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
    ),
    'modSnippet' => 
    array (
    ),
    'modTemplateVar' => 
    array (
    ),
  ),
);